import React  from 'react';
import './style.css';

const Random = (props) => {
        return (
            props.numbers.map((number, index) => {
                return (
                    <div key={index} className="box">
                        <p>{number}</p>
                    </div>
                )
            })
        );
};

export default Random;