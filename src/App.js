import React, { Component } from 'react';
import Random from './components/random/random_component';
import './App.css';

class App extends Component {

    state = {
      numbers: []
    };

    generateNumbers = () => {
      const numbers = [];

            let i = 0;
            while(true){
                const number = Math.floor(Math.random()*32 + 5);
                if(!numbers.includes(number)){
                    numbers.push(number);
                    i++;
                    numbers.sort((a, b) => a - b)
                }
                if(i===5){
                    break;
                }
            }


      let stateNum = [...this.state.numbers];
        stateNum = numbers;
      this.setState({
          numbers: stateNum
      });
    };

  render() {
    return (

        <div className="App">
            <div>
                <button className="btn" onClick={this.generateNumbers}>New numbers</button>
            </div>

            <Random numbers={this.state.numbers}/>


      </div>
    );

  }

}

export default App;
